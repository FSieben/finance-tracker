#!/usr/bin/python

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np


def get_csv_files():
    """Search working directory for csv files and return a list with all files."""
    dirList = os.listdir()
    csv_files = []
    for file_name in dirList:
        if ".csv" in file_name:
            csv_files.append(file_name)
    return csv_files


def choose_csv_file(file_list):
    """Choose a file from "file_list" and return file name."""
    for i, file_name in enumerate(file_list):
        print(f'[{i}] {file_name}', end=' | ')
    print()
    choose = int(input("Choose file to open: "))

    return file_list[choose]


def load_file(file_name):
    """Load file content into pandas DataFrame and return it."""
    return pd.read_csv(file_name, sep='\t')


def draw_finance(Data_Frame):
    """Draw a plot from pandas dataframe."""
    Data_Frame.plot(x=Data_Frame.columns[0]).grid(True)
    plt.show()


def save(data_frame, file_name):
    """Save dataframe into csv file."""
    data_frame.to_csv(file_name, sep = '\t', index = False)


def add_row(Data_Frame, Row):
    """Add a row to Data_Frame."""
    # Convert Date (first column)
    Row[0] = pd.to_datetime(Row[0])
    # Expand list to numbers of columns
    for i in range(len(Row), len(Data_Frame.columns)):
        Row.append(0.)

    Data_Frame.loc[-1] = Row
    Data_Frame.reset_index(drop=True, inplace=True)

    return Data_Frame


def get_row_data(Data_Frame):
    """User input to get the values for the new row."""
    """Return row. If the list is empty. The input was aborted."""
    row = []

    for i in range(len(Data_Frame.columns[0: -4])):
        if i < 1:
            while True:
                value = input(f'Input {Data_Frame.columns[i]}(day first): ')
                if len(value) < 1:
                    return []
                try:
                    value = pd.to_datetime(value, dayfirst=True)
                    break
                except:
                    print("Date in format DD.MM.YYYY")
            row.append(value)
        else:
            while True:
                value = input(f'Input {Data_Frame.columns[i]}: ')
                if len(value) < 1:
                    return []
                try:
                    row.append(float(value))
                    break
                except:
                    print("Input a number.")

    return row


def calculate(data_frame):
    """Calculate the sum, montly dif, monthtly dif in percent, year dif in percent."""
    """Add and calculate the column sum."""
    """Add and calculate the column month €, month % and year %"""
    data_frame['Summe'] = 0
    data_frame['Monat €'] = 0
    data_frame['Monat %'] = 0
    data_frame['Jahr %'] = 0

    for column in data_frame.columns[1:-4]:
        data_frame['Summe'] = data_frame['Summe'] + data_frame[column]

    for i in range(0, len(data_frame['Summe'])):
        if i > 0:
            data_frame.iloc[i, -3] = data_frame.iloc[i, -4] - data_frame.iloc[i-1, -4]
            data_frame.iloc[i, -2] = data_frame.iloc[i, -3] / data_frame.iloc[i-1, -4] * 100
            if i > 11:
                data_frame.iloc[i, -1] = (data_frame.iloc[i, -4] / data_frame.iloc[i-12, -4] - 1) * 100

    return data_frame


def convert_table(data_frame):
    """Convert csv table into pandas table."""
    # Convert strings (1.234,56 €) into float
    for column in range(1, len(data_frame.iloc[0])):
        for row in range(len(data_frame)):
            # Delete each '.' (decimal separator), replace each ',' (comma) with '.' (dot) and remove '€' sign
            data_frame.iloc[row, column] = data_frame.iloc[row, column][:-2].replace('.', '').replace(',', '.')
        # Convert the string in to a float.
        data_frame.iloc[:, column] = data_frame.iloc[:, column].astype(float)

    # Convert the date string into pandas datetime
    data_frame.iloc[:, 0] = pd.to_datetime(data_frame.iloc[:, 0], format='%d.%m.%Y')
    return data_frame


def menu():
    """Draw the menu."""
    print("Load...............[0]")
    print("Save...............[1]")
    print("Save As............[2]")
    print("Calculate..........[3]")
    print("Plot...............[4]")
    print("Show table.........[5]")
    print("Data Input.........[6]")
    print("Exit..............[10]")
    while True:
        choose = input("Choose: ")
        try:
            choose = int(choose)
            break
        except:
            print("Input a number.")
    return choose


def main():
    """Main programm for finance tracker."""
    run_program = True
    while run_program:
        menu_choose = menu()
        if menu_choose == 0:
            # Load file.
            file_name = choose_csv_file(get_csv_files())
            data_frame = load_file(file_name)
            # Convert table to a corect table format.
            data_frame = convert_table(data_frame)
        elif menu_choose == 1:
            # Save
            save(data_frame, file_name)
        elif menu_choose == 2:
            # Save As
            new_file_name = input("File name: ")
            if len(new_file_name) > 0:
                save(data_frame, new_file_name)
            else:
                print("No file choosen.")
        elif menu_choose == 3:
            calculate(data_frame)
        elif menu_choose == 4:
            for i, col in enumerate(data_frame.columns):
                print(f"{i}: {col}", end=" - ")
            row_choose = int(input("Wich row to plot: "))
            draw_finance(data_frame[[data_frame.columns[0], data_frame.columns[row_choose]]])
        elif menu_choose == 5:
            print(calculate(data_frame))
        elif menu_choose == 6:
            row = get_row_data(data_frame)
            if len(row) > 3:
                data_frame = add_row(data_frame, row)
        elif menu_choose == 10:
            run_program = False


if __name__ == '__main__':
    main()
